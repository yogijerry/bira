package sordsman.com.bira;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by eit on 4/28/15.
 */
public class SplashActivity extends Activity {
    //duration of splash screen
    long Delay = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //get view from activity_splash.xml
        setContentView(R.layout.activity_splash);

        //create timer
        Timer RunSplash = new Timer();

        //when timer ends
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                //close splashscreen activity


                //start main activity
                Intent myIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(myIntent);


            }
        };
        RunSplash.schedule(ShowSplash, Delay);
    }
}
